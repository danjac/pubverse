import pytest
from django.contrib.auth.models import AnonymousUser
from django.test import Client

from pubverse.activitypub.models import Actor
from pubverse.activitypub.tests.factories import create_actor
from pubverse.users.models import User
from pubverse.users.tests.factories import create_user


@pytest.fixture()
def user() -> User:
    return create_user()


@pytest.fixture()
def user_with_actor(user: User) -> User:
    user.actor = create_actor(
        user=user,
        name=user.username,
        actor_type=Actor.ActorType.PERSON,
    )
    return user


@pytest.fixture()
def anonymous_user() -> AnonymousUser:
    return AnonymousUser()


@pytest.fixture()
def auth_user(client: Client, user: User) -> User:
    client.force_login(user)
    return user


@pytest.fixture()
def staff_user(client: Client) -> User:
    user = create_user(is_staff=True)
    client.force_login(user)
    return user
