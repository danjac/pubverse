import pytest

from pubverse.posts.models import Post
from pubverse.posts.tests.factories import create_post


@pytest.fixture()
def post() -> Post:
    return create_post()
