from faker import Faker

from pubverse.activitypub.models import Actor, Domain
from pubverse.activitypub.tests.factories import (
    create_actor,
    create_domain,
    create_group_actor,
)
from pubverse.posts.models import Post
from pubverse.tests.factories import NotSet, resolve

_faker = Faker()


def create_post(
    *,
    owner: Actor = NotSet,
    community: Actor = NotSet,
    domain: Domain = NotSet,
    url: str | None = NotSet,
    description: str | None = NotSet,
    **kwargs,
) -> Post:
    return Post.objects.create(
        owner=resolve(owner, create_actor),
        community=resolve(community, create_group_actor),
        domain=resolve(domain, create_domain),
        url=resolve(url, _faker.url),
        description=resolve(description, _faker.text),
        **kwargs,
    )
