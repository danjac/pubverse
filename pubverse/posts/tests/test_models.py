from pubverse.posts.models import Post


class TestPost:
    def test_str_url(self):
        post = Post(url="https://example.com", description="test")
        assert str(post) == post.url

    def test_str_description(self):
        post = Post(description="test")
        assert str(post) == post.description
