from __future__ import annotations

from typing import TYPE_CHECKING

from django.db import models
from model_utils.models import TimeStampedModel

if TYPE_CHECKING:  # pragma: no cover
    from datetime import datetime

    from pubverse.activitypub.models import Actor, Domain


class Post(TimeStampedModel):
    """Represents posted URL or content."""

    owner: Actor = models.ForeignKey(
        "activitypub.Actor",
        related_name="owned_posts",
        on_delete=models.PROTECT,
    )

    domain: Domain = models.ForeignKey(
        "activitypub.Domain",
        related_name="posts",
        on_delete=models.PROTECT,
    )

    community: Actor = models.ForeignKey(
        "activitypub.Actor",
        related_name="community_posts",
        on_delete=models.PROTECT,
    )

    thread: Post = models.ForeignKey(
        "self",
        related_name="comments",
        on_delete=models.PROTECT,
    )

    parent: Post = models.ForeignKey(
        "self",
        related_name="children",
        on_delete=models.PROTECT,
    )

    # for remote content
    object_id: str | None = models.URLField(
        null=True,
        blank=True,
        unique=True,
    )

    # for remote content
    object_url: str | None = models.URLField(null=True, blank=True)

    url: str | None = models.URLField(null=True, blank=True)
    description: str = models.TextField(blank=True)

    deleted: datetime | None = models.DateTimeField(null=True, blank=True)

    comments_allowed = models.BooleanField(default=True)
    nsfw: bool = models.BooleanField(default=False)

    def __str__(self) -> str:
        """Returns URL or description of post."""
        return self.url or self.description
