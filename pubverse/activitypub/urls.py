from django.urls import path

from pubverse.activitypub import views

app_name = "activitypub"

urlpatterns = [
    path("@<slug:name>/", views.actor_object, name="object"),
    path("@<slug:name>/inbox", views.inbox, name="inbox"),
    path("@<slug:name>/outbox", views.outbox, name="outbox"),
    path("@<slug:name>/followers", views.followers, name="followers"),
    path(
        "@<slug:name>/following",
        views.followers,
        name="following",
        kwargs={"following": True},
    ),
    path(".well-known/webfinger", views.webfinger, name="webfinger"),
]
