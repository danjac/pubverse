import pytest
from django.urls import reverse, reverse_lazy

from pubverse.tests.asserts import assert_200, assert_404


class TestActorObject:
    @pytest.mark.django_db()
    def test_actor(self, client, actor):
        response = client.get(
            reverse("activitypub:object", args=[actor.name]),
        )

        assert_200(response)

    @pytest.mark.django_db()
    def test_group_actor(self, client, group_actor):
        response = client.get(
            reverse("activitypub:object", args=[group_actor.name]),
        )

        assert_200(response)

    @pytest.mark.django_db()
    def test_federated_actor(self, client, remote_actor):
        response = client.get(
            reverse("activitypub:object", args=[remote_actor.name]),
        )
        assert_404(response)


class TestWebfinger:
    url = reverse_lazy("activitypub:webfinger")

    @pytest.mark.django_db()
    def test_invalid_resource(self, client):
        response = client.get(
            self.url,
            {
                "resource": "acct:testme",
            },
        )
        assert_404(response)

    @pytest.mark.django_db()
    def test_local_domain(self, client, actor):
        response = client.get(
            self.url,
            {
                "resource": f"acct:{actor.name}@testserver",
            },
        )
        assert_200(response)

    @pytest.mark.django_db()
    def test_federated_domain(self, client, remote_actor):
        response = client.get(
            self.url,
            {
                "resource": f"acct:{remote_actor.name}@{remote_actor.domain.host}",
            },
        )
        assert_404(response)
