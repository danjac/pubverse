import pytest

from pubverse.activitypub.models import Actor, Domain
from pubverse.activitypub.tests.factories import (
    create_actor,
    create_domain,
    create_group_actor,
)


@pytest.fixture()
def domain() -> Domain:
    return create_domain(local=True, host="testserver")


@pytest.fixture()
def remote_domain() -> Domain:
    return create_domain(local=False)


@pytest.fixture()
def actor(domain) -> Actor:
    return create_actor(domain=domain)


@pytest.fixture()
def group_actor(domain) -> Actor:
    return create_group_actor(domain=domain)


@pytest.fixture()
def remote_actor(remote_domain) -> Actor:
    return create_actor(domain=remote_domain)


@pytest.fixture()
def remote_group_actor(remote_domain) -> Actor:
    return create_group_actor(domain=remote_domain)
