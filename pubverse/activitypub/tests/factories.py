import functools

from faker import Faker

from pubverse.activitypub.models import Actor, Domain
from pubverse.tests.factories import NotSet, resolve

_faker = Faker()


def create_domain(*, host: str = NotSet, **kwargs) -> Domain:
    return Domain.objects.create(
        host=resolve(host, _faker.unique.domain_name),
        **kwargs,
    )


def create_actor(
    *,
    name: str = NotSet,
    actor_type: str = Actor.ActorType.PERSON,
    object_id: str = NotSet,
    domain: Domain = NotSet,
    **kwargs,
) -> Actor:
    return Actor.objects.create(
        name=resolve(name, _faker.unique.user_name),
        object_id=resolve(object_id, _faker.unique.url),
        domain=resolve(domain, create_domain),
        **kwargs,
    )


create_group_actor = functools.partial(create_actor, actor_type=Actor.ActorType.GROUP)
