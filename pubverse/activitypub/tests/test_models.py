from pubverse.activitypub.models import Actor, Domain


class TestDomain:
    def test_str(self):
        assert str(Domain(host="example.com")) == "example.com"


class TestActor:
    def test_str(self):
        assert (
            str(Actor(name="tester", domain=Domain(host="example.com")))
            == "tester@example.com"
        )
