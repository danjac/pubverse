from django.core.paginator import Paginator
from django.http import Http404, HttpRequest, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.decorators.http import require_safe

from pubverse.activitypub.models import Actor
from pubverse.posts.models import Post


@require_safe
def actor_object(request: HttpRequest, name: str) -> JsonResponse:
    """Renders Person or Group object.

    URL looks like:

        https://example.com/@johndoe -> points to Person "johndoe"
        https://example.com/@ufos -> points to Group "ufos"

    The only URL we need to handle depending on type is the profile URL,
    which is part of the UI. The other links (inbox etc) are type-agnostic.

    The URL pattern is:

        /u/{name} -> Persons (i.e. Users)
        /c/{name} -> Groups (i.e. Communities)
        /@{name} -> Actors (i.e. Persons or Groups)
    """

    actor = get_object_or_404(
        Actor.objects.select_related("domain"),
        domain__host=request.get_host(),
        name=name,
    )

    object_id = request.build_absolute_uri()

    public_key = (
        {
            "public_key": {
                "owner": object_id,
                "id": f"{object_id}#main-key",
                "publicKeyPrem": actor.public_key,
            }
        }
        if actor.public_key
        else {}
    )

    return JsonResponse(
        {
            "@context": [
                "https://www.w3.org/ns/activitystreams",
                "https://w3id.org/security/v1",
                {
                    "manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
                    "schema": "http://schema.org#",
                    "PropertyValue": "schema:PropertyValue",
                    "value": "schema:value",
                },
            ],
            "id": object_id,
            "type": actor.actor_type,
            "name": actor.name,
            "preferred_username": actor.preferred_username,
            "manuallyApprovesFollowers": actor.manually_approves_followers,
            "sensitive": actor.sensitive,
            "published": actor.published,
            "url": object_id,
            "inbox": actor.get_inbox_uri(),
            "outbox": actor.get_outbox_uri(),
            "followers": actor.get_followers_uri(),
            "following": actor.get_following_uri(),
            **public_key,
        },
        content_type="application/activity+json",
    )


def inbox(request: HttpRequest, name: str) -> JsonResponse:
    """Handles inbox."""
    return JsonResponse()


@require_safe
def outbox(request: HttpRequest, name: str) -> JsonResponse:
    """Responds with ActivityPub outbox."""

    actor = get_object_or_404(
        Actor,
        domain__host=request.get_host(),
        name=name,
    )

    # NOTE: at this point, all posts are Public, this will change

    posts = Post.objects.filter(
        published__isnull=False,
        deleted__isnull=True,
    )
    posts = (
        posts(community=actor)
        if actor.actor_type == Actor.ActorType.GROUP
        else posts(owner=actor)
    )

    object_id = request.build_absolute_uri(request.path)

    try:
        page = int(request.GET.get("page"))
    except (KeyError, IndexError):
        page = None

    if page is None:
        return JsonResponse(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "type": "OrderedCollection",
                "id": object_id,
                "totalItems": posts.count(),
                "first": f"{object_id}?page=1",
            },
            content_type="application/activity+json",
        )

    page_obj = Paginator(posts, 10).get_page(page)

    items: list[dict] = [
        {
            "@context": [
                "https://www.w3.org/ns/activitystreams",
                "https://w3id.org/security/v1",
            ],
            "type": "Create",
            "id": post.get_object_id(request),
            "actor": actor.get_object_id(request),
            "published": post.published,
            "to": [
                # if public
                "https://www.w3.org/ns/activitystreams#Public",
                # @mentions
            ],
            "cc": [
                actor.followers_uri
                or request.build_absolute_uri(
                    reverse("actors:followers", args=[actor.name]),
                )
            ],
            "content": post.description,
            "mediaType": "text/html",
            "url": post.get_absolute_url(request),
            # tags and mentions
        }
        for post in page_obj
    ]

    return JsonResponse(
        {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "OrderedCollectionPage",
            "part_of": object_id,
            "id": request.build_absolute_uri(),
            "totalItems": page_obj.paginator.count,
            "next": f"{object_id}?page=={page_obj.next_page()}",
            "orderedItems": items,
        }
    )


@require_safe
def followers(
    request: HttpRequest, name: str, *, following: bool = False
) -> JsonResponse:
    """Followed or following actors."""
    actor = get_object_or_404(
        Actor,
        domain__host=request.get_host(),
        name=name,
    )

    qs = actor.following.all() if following else actor.followers.all()
    qs = qs.order_by("-published", "-created").select_related("domain")

    object_id = request.build_absolute_uri()

    try:
        page = int(request.GET.get("page"))
    except (KeyError, IndexError):
        page = None

    if page is None:
        return JsonResponse(
            {
                "@context": "https://www.w3.org/ns/activitystreams",
                "type": "OrderedCollection",
                "id": object_id,
                "totalItems": qs.count(),
                "first": f"{object_id}?page=1",
            },
            content_type="application/activity+json",
        )

    page_obj = Paginator(qs, 10).get_page(page)

    items: list[dict] = [
        {
            "@context": [
                "https://www.w3.org/ns/activitystreams",
                "https://w3id.org/security/v1",
            ],
            "id": object_id,
            "partOf": object_id,
            "orderedItems": [item.get_object_id() for item in page_obj],
        }
        for post in page_obj
    ]

    return JsonResponse(
        {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "OrderedCollectionPage",
            "part_of": request.build_absolute_uri(request.path),
            "id": object_id,
            "totalItems": page_obj.paginator.count,
            "next": f"{object_id}?page=={page_obj.next_page()}",
            "orderedItems": items,
        }
    )


@require_safe
def webfinger(request: HttpRequest) -> JsonResponse:
    """Return Webfinger info.

    See https://fedidevs.org/projects/kbin/webfinger
    """
    try:
        resource = request.GET["resource"].split("acct:")[1]
        name, domain = resource.split("@")
    except (KeyError, ValueError) as e:
        raise Http404 from e

    actor = get_object_or_404(
        Actor.objects.select_related("domain"),
        domain__local=True,
        domain__host=domain,
        name=name,
    )

    object_id = actor.get_object_id()

    return JsonResponse(
        {
            "subject": resource,
            "aliases": [object_id],
            "links": [
                {
                    "rel": "http://webfinger.net/rel/profile-page",
                    "href": object_id,
                    "type": "text/html",
                },
                {
                    "rel": "self",
                    "href": object_id,
                    "type": "application/activity+json",
                },
            ],
        },
        content_type="application/jrd+json",
    )
