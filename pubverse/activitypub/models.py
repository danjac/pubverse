from __future__ import annotations

from typing import TYPE_CHECKING, ClassVar
from urllib.parse import urljoin

from django.conf import settings
from django.db import models
from django.urls import reverse
from model_utils.models import TimeStampedModel

if TYPE_CHECKING:  # pragma: no cover
    from datetime import datetime

    from pubverse.users.models import User


class Domain(TimeStampedModel):
    """Represents an instance domain."""

    host: str = models.CharField(max_length=100, unique=True)
    local: bool = models.BooleanField(default=True)
    blocked: bool = models.BooleanField(default=False)

    def __str__(self) -> str:
        """Returns hostname."""
        return self.host


class Actor(TimeStampedModel):
    """Represents an ActivityPub identity/actor.

    How to handle followers between local/remote?
    """

    class ActorType(models.TextChoices):
        PERSON = "Person"
        GROUP = "Group"

        # these choices are not used by Pubverse but maybe by federated instances
        SERVICE = "Service"
        APPLICATION = "Application"
        ORGANIZATION = "Organization"

    name: str = models.SlugField()
    preferred_username: str = models.CharField(max_length=200, blank=True)

    domain: Domain = models.ForeignKey(
        Domain,
        related_name="actors",
        on_delete=models.PROTECT,
    )

    # Auth User (local Person Actor)
    user: User | None = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="actor",
        on_delete=models.PROTECT,
    )

    # Community Owner (local Group Actor)
    owner: User | None = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="owner",
        on_delete=models.PROTECT,
    )

    # Community Mods (local Group Actor)
    moderators: models.QuerySet[User] = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="moderated_communities",
    )

    published: datetime | None = models.DateTimeField(null=True, blank=True)

    actor_type: str = models.CharField(max_length=12, choices=ActorType.choices)
    summary: str = models.TextField(blank=True)

    followers: models.QuerySet[Actor] = models.ManyToManyField("self", blank=True)
    following: models.QuerySet[Actor] = models.ManyToManyField("self", blank=True)

    public_key: str = models.TextField(blank=True)
    private_key: str = models.TextField(blank=True)

    # Identity fields (Remote Actors)
    object_id: str = models.URLField(unique=True, null=True, blank=True)
    profile_uri: str | None = models.URLField(null=True, blank=True)
    inbox_uri: str | None = models.URLField(null=True, blank=True)
    outbox_uri: str | None = models.URLField(null=True, blank=True)
    following_uri: str | None = models.URLField(null=True, blank=True)
    followers_uri: str | None = models.URLField(null=True, blank=True)
    moderators_uri: str | None = models.URLField(null=True, blank=True)

    # blocked for whole site
    blocked: bool = models.BooleanField(default=False)
    sensitive: bool = models.BooleanField(default=False)
    discoverable: bool = models.BooleanField(default=True)
    manually_approves_followers: bool = models.BooleanField(default=True)

    # TBD: need a "deleted" and "active" fields

    class Meta:
        constraints: ClassVar[list] = [
            models.UniqueConstraint(
                fields=["name", "domain"],
                name="%(app_label)s_%(class)s_uniq_name_domain",
            )
        ]

    def __str__(self) -> str:
        """Return the object ID."""
        return f"{self.name}@{self.domain}"

    def get_object_id(self) -> str:
        """Returns absolute URI to object ID."""
        return self.object_id or self._build_absolute_uri(
            reverse("activitypub:object", args=[self.name])
        )

    def get_inbox_uri(self) -> str:
        """Returns absolute URI to inbox endpoint."""
        return self.inbox_uri or self._build_absolute_uri(
            reverse("activitypub:inbox", args=[self.name])
        )

    def get_outbox_uri(self) -> str:
        """Returns absolute URI to outbox endpoint."""
        return self.outbox_uri or self._build_absolute_uri(
            reverse("activitypub:outbox", args=[self.name])
        )

    def get_followers_uri(self) -> str:
        """Returns absolute URI to followers endpoint."""
        return self.followers_uri or self._build_absolute_uri(
            reverse("activitypub:followers", args=[self.name])
        )

    def get_following_uri(self) -> str:
        """Returns absolute URI to followers endpoint."""
        return self.following_uri or self._build_absolute_uri(
            reverse("activitypub:following", args=[self.name])
        )

    def _build_absolute_uri(self, url: str) -> str:
        scheme = "https" if settings.USE_HTTPS else "http"
        return urljoin(f"{scheme}://{self.domain.host}", url)
