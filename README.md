This is a simple link aggregator based on ActivityPub.

This project is an exploration of ActivityPub and the Fediverse. It is loosely based on "Reddit clones" such as Lemmy or Kbin, and inspired by [Tildes](https://tildes.net).

**It is not intended for production: I recommend you to use Lemmy, Kbin or other maintained and supported projects instead.**

## Features

These are the planned features of the project:

* Users
* Topics
* Posts
* Comments
* Boosts
* Mentions
* Likes
* Hashtags
* Follow/Accept/Reject/Undo (tags, people)
* Moderators (per topic)
* Admins
* Dark/light mode
* Webfinger
* Markdown

Things we won't be doing, at least in the short term:

* User media content: e.g. avatars, photos, documents. This is to avoid having to handle media uploads, S3 integration costs etc.
* Microblogging: we will just have posts/threads and comments, no Mastodon-style toots.
* Themes: there will be dark/light modes based on browser settings, but no preset or user-defined themes.
* Direct messages: DMs can be added later when ActivityPub integration is further along, but we will not include for now.

## Tech stack

* Python
* Django
* PostgresSQL
* Redis
* HTMX
* AlpineJS
* Tailwind

[RQ](https://pypi.org/project/django-rq/) can be used for queues with Redis. For example, publishing a new Post across the Fediverse is a fairly expensive action, so can be handed off to a queued job.

## Local development

_TBD_

## Deployment

The "happy path" of deployment is using a PAAS such as Heroku or fly.io, or self-hosted such as [Dokku](https://dokku.com). A Dockerfile has been included so that other deployments should be as painless as possible.

In addition to the Docker image, the application requires PostgresSQL 14+ and Redis 7+. These may be provided by your PAAS or as separate services such as Amazon RDS.

The following environment variables should be set in your production installation (changing `pubverse.social` for your domain) using whatever provided environment or secrets manager.

```
ALLOWED_HOSTS=pubverse.social
CONN_MAX_AGE=360
DATABASE_URL=<database-url>
REDIS_URL=<redis-url>
ADMIN_URL=<admin-url>
ADMINS=me@pubverse.social
EMAIL_HOST=mg.pubverse.social
MAILGUN_API_KEY=<mailgun_api_key>
SECRET_KEY=<secret>
SENTRY_URL=<sentry-url>
```

Some PAAS e.g. Heroku automatically set some settings e.g. `DATABASE_URL`.

You should only need to set Mailgun and Sentry settings if you are using these services, they are not required for development.

## User interface

Pubverse should be fully accessible and follow [web accessibility guidelines](https://www.w3.org/WAI/standards-guidelines/).

The site should be responsive, available in phone, tablet and desktop ratios.

The UI has dark/light modes based on browser settings, but there are no user-defined or preset themes at this point.

Localization should be fully supported. While English will be the only supported language in the early development stage, code should be written to support other languages when translations are made available (e.g. using Django i18n functionality).

## URL design

* _/users/danjac_: points to local User "danjac"
* _/topics/movies_: points to local Topic "movies"
* _/users/danjac@lemmy.ml_: points to User(Actor) "danjac" on "lemmy.ml" instance
* _/topics/movies@lemmy.ml_: points to Topic(Group) "movies" on "lemmy.ml" instance
* _/tags/fridaycatpics_: points to Hashtag "#fridaycatpics"

## Functionality

An **Instance** is equivalent to a single domain. While in theory we could have multiple domains on a single instance, to keep things simple we will just support a single domain at this point.

**Users** should be able to, at minimum, create Posts and Comments. A user may be banned from an Instance or a Topic, in which case they cannot create content on that site or Topic.

Users can also be Moderators or Admins. A Moderator has certain powers inside one or more Topics, such as being able to ban users, remove or lock Posts, and remove Comments.

User login should support at least basic username/email/password authentication, with optional support for e.g. Google auth.

Admins have greater powers across the whole Instance. They can ban users entirely, or configure instance settings.

Content is categorized **Topics**. Topics are equivalent to subreddits, or communities in Lemmy, or magazines in Kbin.

All Posts belong to a Topic. A Topic has its own moderators, and its own settings.

**Posts** are the basic unit of content; in other platforms they may be called "threads". A Post can include a link and/or description, and must have a title. Hashtags can be appended to a Post.

**Comments** belong to a Post. Any user who can post to the Topic should also be able to comment on a Post. A Post can be locked, so that Comments can no longer be posted.

Posts should support "soft deleting". A Post that has comments should not be deletable, but instead if "deleted" the link/description should be scrubbed, so other users' Comments are not lost.

It should be possible to mark Posts "NSFW" and users should be able to filter NSFW posts. Topics can also be blanket marked NSFW and/or require age verification.

Moderators should never be able to directly edit a Post or Comment. They may delete a Post or Comment, or lock Comments or hide Comments entirely in a Post, but cannot change content written by another user.

Users can vote on (or "Like") all Posts and Comments by default. Topic settings may only permit Members from voting.

Users can boost Posts and Comments. Boosted Posts and Comments are given more visibility.

Posts on the home page, or each Topic page should be orderable by date, likes, comment date or boosts.

Comments inside a Post should be orderable by date, likes etc.

Users can be mentioned in Posts and Comments using the common "@" syntax.

Each user profile should show their Posts and Comments.

Anonymous users should be able to have read-only access throughout the Instance. This can be changed at Instance level, i.e. access granted only to authenticated users, or even approved authenticated users.

The same settings can be applied to Topics: a Topic may require age verification, approved membership etc.

Post and Comments can use Markdown. We don't need to support rich text editors at this point.

Search functionality can be implemented with PostgresSQL full text search, and should allow searching across Posts, Comments, Users and Topics.

## Notifications

Examples:

* Follow request
* Post commmented
* Comment replied
* Post or comment flagged (Moderators)
* User requests to join Topic (Moderators)

Notifications should be optionally delivered by email per notification. All Notifications should be visible to current user on some page/tab in the UI.

## ActivityPub

An **Actor** can be represented as a User(=**Person**) or Topic (=**Group**).

Users should be able to **Follow** groups and persons, **Accept** or **Reject** follows, or **Undo** follows.

Posts and Comments are represented as a **Note**.

Boosts are handled as an **Announce**.

Likes, hashtags and mentions are also handled.

**Webfinger** can be used to support discoverability of User/Persons and Topic/Groups.

The aim is to have 100% interopability with other Pubverse instances, and close interop with Lemmy and Kbin, with weaker but still at least some interop with Mastodon.

The [Kbin documentation](https://fedidevs.org/projects/kbin/) can be used as a guide.

Federation should be adjustable by Instance admins. As an admin, I should be able to decide which instances to federate with or block, indeed whether to allow federation at all.

It should therefore be possible to import Posts, Comments and Topics from other instances.

Multiple Topics with the same name can co-exist in Pubverse, as long as they are unique by URL. In other words, you can have a local Topic called _/topics/movies_, but only one such Topic, but you can also have _/topics/movies@lemmy.ml_.
