pytest_plugins = [
    "pubverse.tests.fixtures",
    "pubverse.activitypub.tests.fixtures",
    "pubverse.posts.tests.fixtures",
    "pubverse.users.tests.fixtures",
]
