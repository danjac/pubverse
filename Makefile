install: pyinstall npminstall precommitinstall

dbinstall: migrate fixtures

update: pyupdate pyexport npmupdate precommitupdate

compose:
	docker-compose up -d --build

pyinstall:
	poetry install

pyupdate:
	poetry update

pyexport:
	poetry export -o requirements.txt --without-hashes
	poetry export -o requirements-ci.txt --with=dev --without-hashes

npminstall:
	npm ci

npmupdate:
	npm run check-updates && npm install

precommitinstall:
	pre-commit install

precommitupdate:
	pre-commit autoupdate

migrate:
	python ./manage.py migrate

serve:
	python ./manage.py runserver

shell:
	python ./manage.py shell_plus

build:
	npm run build

watch:
	npm run watch

test:
	python -m pytest

clean:
	git clean -Xdf
